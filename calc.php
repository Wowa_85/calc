<html>
<head>
	<meta charset="UTF-8">
	<title> Калькулятор</title>
</head>
<body>
<?php

$output = '';
$expression = '';
$error = '';
$number1 = '';
$number2 = '';
$operator = '';

function clearData($data, $type = 'i')
{
	switch($type) {
		case 'i':
			return (int) $data;
			break;
		case 's':
			return trim(strip_tags($data));
			break;
	}
}

if($_SERVER ['REQUEST_METHOD'] == "POST") {
	$number1 = isset($_POST['number1']) ?  clearData($_POST['number1']) : null;
	$number2 = isset($_POST['number2']) ?  clearData($_POST['number2']) : null;
	$operator = isset($_POST['operator']) ?  clearData($_POST['operator'], 's') : null;
	switch($operator) {
		case '+':
			$output = $number1 + $number2;
			break;
		case '-':
			$output = $number1 - $number2;
			break;
		case '*':
			$output = $number1 * $number2;
			break;
		case '/':
			if($number2 == 0) {
				$error = 'Ділення на 0 заборонена!';
			}
			else {
				$output = $number1 / $number2;
			}
			break;
		default:
			if(!empty($operator)) {
				$error = "Не відомий оператор '$operator'";
			}

			break;
	}

	if(empty($error) && $number1 !== '' && !empty($operator) && $number2 !== '') {
		$expression = $number1 . $operator . $number2 . ' = ' . $output;
	}
}
?>

<form action="<?=$_SERVER['PHP_SELF']?>" method="post">
	Число 1: <br/>
	<input type="text" name="number1" value="<?php echo $number1;?>"> <br/>
	Оператор: <br/>
	<input type="text" name="operator" value="<?php echo $operator;?>"> <br/>
	Число 2: <br/>
	<input type="text" name="number2" value="<?php echo $number2;?>"> <br/>
	<br/>
	<input type="submit" value="Рахувати"> <br/>
</form>
<?php echo $expression;?>
<p style="color: red;"><?php echo $error;?></p>

</body>
</html>